package ru.tsc.golovina.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.endpoint.Task;
import ru.tsc.golovina.tm.exception.entity.TaskNotFoundException;

import java.util.Optional;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus());
    }

}
