package ru.tsc.golovina.tm.endpoint;

import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-05-27T19:16:37.871+05:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", name = "UserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface UserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/registryUserRequest", output = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/registryUserResponse")
    @RequestWrapper(localName = "registryUser", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.RegistryUser")
    @ResponseWrapper(localName = "registryUserResponse", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.RegistryUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.tsc.golovina.tm.endpoint.Session registryUser(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/getCurrentUserRequest", output = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/getCurrentUserResponse")
    @RequestWrapper(localName = "getCurrentUser", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.GetCurrentUser")
    @ResponseWrapper(localName = "getCurrentUserResponse", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.GetCurrentUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.tsc.golovina.tm.endpoint.User getCurrentUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.golovina.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/updateUserByIdRequest", output = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/updateUserByIdResponse")
    @RequestWrapper(localName = "updateUserById", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.UpdateUserById")
    @ResponseWrapper(localName = "updateUserByIdResponse", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.UpdateUserByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.tsc.golovina.tm.endpoint.User updateUserById(
            @WebParam(name = "session", targetNamespace = "")
                    Session session,
            @NotNull String id, @WebParam(name = "firstName", targetNamespace = "")
                    String firstName,
            @WebParam(name = "lastName", targetNamespace = "")
                    String lastName,
            @WebParam(name = "middleName", targetNamespace = "")
                    String middleName,
            @WebParam(name = "email", targetNamespace = "")
                    String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/findAllUserRequest", output = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/findAllUserResponse")
    @RequestWrapper(localName = "findAllUser", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.FindAllUser")
    @ResponseWrapper(localName = "findAllUserResponse", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.FindAllUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.tsc.golovina.tm.endpoint.User> findAllUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.golovina.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/updateUserPasswordRequest", output = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/updateUserPasswordResponse")
    @RequestWrapper(localName = "updateUserPassword", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.UpdateUserPassword")
    @ResponseWrapper(localName = "updateUserPasswordResponse", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.UpdateUserPasswordResponse")
    public void updateUserPassword(
        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.golovina.tm.endpoint.Session session,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/existsUserByLoginRequest", output = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/existsUserByLoginResponse")
    @RequestWrapper(localName = "existsUserByLogin", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.ExistsUserByLogin")
    @ResponseWrapper(localName = "existsUserByLoginResponse", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.ExistsUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean existsUserByLogin(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/existsUserByEmailRequest", output = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/existsUserByEmailResponse")
    @RequestWrapper(localName = "existsUserByEmail", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.ExistsUserByEmail")
    @ResponseWrapper(localName = "existsUserByEmailResponse", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.ExistsUserByEmailResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean existsUserByEmail(
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/findUserByIdRequest", output = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/findUserByIdResponse")
    @RequestWrapper(localName = "findUserById", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.FindUserById")
    @ResponseWrapper(localName = "findUserByIdResponse", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.FindUserByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.tsc.golovina.tm.endpoint.User findUserById(
        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.golovina.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/clearUserRequest", output = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/clearUserResponse")
    @RequestWrapper(localName = "clearUser", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.ClearUser")
    @ResponseWrapper(localName = "clearUserResponse", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.ClearUserResponse")
    public void clearUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.golovina.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/updateUserByLoginRequest", output = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/updateUserByLoginResponse")
    @RequestWrapper(localName = "updateUserByLogin", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.UpdateUserByLogin")
    @ResponseWrapper(localName = "updateUserByLoginResponse", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.UpdateUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.tsc.golovina.tm.endpoint.User updateUserByLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.golovina.tm.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "firstName", targetNamespace = "")
        java.lang.String firstName,
        @WebParam(name = "lastName", targetNamespace = "")
        java.lang.String lastName,
        @WebParam(name = "middleName", targetNamespace = "")
        java.lang.String middleName,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/findUserByLoginRequest", output = "http://endpoint.tm.golovina.tsc.ru/UserEndpoint/findUserByLoginResponse")
    @RequestWrapper(localName = "findUserByLogin", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.FindUserByLogin")
    @ResponseWrapper(localName = "findUserByLoginResponse", targetNamespace = "http://endpoint.tm.golovina.tsc.ru/", className = "ru.tsc.golovina.tm.endpoint.FindUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.tsc.golovina.tm.endpoint.User findUserByLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.tsc.golovina.tm.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );
}
