
package ru.tsc.golovina.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tsc.golovina.tm.endpoint package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ClearUser_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "clearUser");
    private final static QName _ClearUserResponse_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "clearUserResponse");
    private final static QName _ExistsUserByEmail_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "existsUserByEmail");
    private final static QName _ExistsUserByEmailResponse_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "existsUserByEmailResponse");
    private final static QName _ExistsUserByLogin_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "existsUserByLogin");
    private final static QName _ExistsUserByLoginResponse_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "existsUserByLoginResponse");
    private final static QName _FindAllUser_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "findAllUser");
    private final static QName _FindAllUserResponse_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "findAllUserResponse");
    private final static QName _FindUserById_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "findUserById");
    private final static QName _FindUserByIdResponse_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "findUserByIdResponse");
    private final static QName _FindUserByLogin_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "findUserByLogin");
    private final static QName _FindUserByLoginResponse_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "findUserByLoginResponse");
    private final static QName _GetCurrentUser_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "getCurrentUser");
    private final static QName _GetCurrentUserResponse_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "getCurrentUserResponse");
    private final static QName _RegistryUser_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "registryUser");
    private final static QName _RegistryUserResponse_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "registryUserResponse");
    private final static QName _UpdateUserById_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "updateUserById");
    private final static QName _UpdateUserByIdResponse_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "updateUserByIdResponse");
    private final static QName _UpdateUserByLogin_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "updateUserByLogin");
    private final static QName _UpdateUserByLoginResponse_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "updateUserByLoginResponse");
    private final static QName _UpdateUserPassword_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "updateUserPassword");
    private final static QName _UpdateUserPasswordResponse_QNAME = new QName("http://endpoint.tm.golovina.tsc.ru/", "updateUserPasswordResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tsc.golovina.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ClearUser }
     * 
     */
    public ClearUser createClearUser() {
        return new ClearUser();
    }

    /**
     * Create an instance of {@link ClearUserResponse }
     * 
     */
    public ClearUserResponse createClearUserResponse() {
        return new ClearUserResponse();
    }

    /**
     * Create an instance of {@link ExistsUserByEmail }
     * 
     */
    public ExistsUserByEmail createExistsUserByEmail() {
        return new ExistsUserByEmail();
    }

    /**
     * Create an instance of {@link ExistsUserByEmailResponse }
     * 
     */
    public ExistsUserByEmailResponse createExistsUserByEmailResponse() {
        return new ExistsUserByEmailResponse();
    }

    /**
     * Create an instance of {@link ExistsUserByLogin }
     * 
     */
    public ExistsUserByLogin createExistsUserByLogin() {
        return new ExistsUserByLogin();
    }

    /**
     * Create an instance of {@link ExistsUserByLoginResponse }
     * 
     */
    public ExistsUserByLoginResponse createExistsUserByLoginResponse() {
        return new ExistsUserByLoginResponse();
    }

    /**
     * Create an instance of {@link FindAllUser }
     * 
     */
    public FindAllUser createFindAllUser() {
        return new FindAllUser();
    }

    /**
     * Create an instance of {@link FindAllUserResponse }
     * 
     */
    public FindAllUserResponse createFindAllUserResponse() {
        return new FindAllUserResponse();
    }

    /**
     * Create an instance of {@link FindUserById }
     * 
     */
    public FindUserById createFindUserById() {
        return new FindUserById();
    }

    /**
     * Create an instance of {@link FindUserByIdResponse }
     * 
     */
    public FindUserByIdResponse createFindUserByIdResponse() {
        return new FindUserByIdResponse();
    }

    /**
     * Create an instance of {@link FindUserByLogin }
     * 
     */
    public FindUserByLogin createFindUserByLogin() {
        return new FindUserByLogin();
    }

    /**
     * Create an instance of {@link FindUserByLoginResponse }
     * 
     */
    public FindUserByLoginResponse createFindUserByLoginResponse() {
        return new FindUserByLoginResponse();
    }

    /**
     * Create an instance of {@link GetCurrentUser }
     * 
     */
    public GetCurrentUser createGetCurrentUser() {
        return new GetCurrentUser();
    }

    /**
     * Create an instance of {@link GetCurrentUserResponse }
     * 
     */
    public GetCurrentUserResponse createGetCurrentUserResponse() {
        return new GetCurrentUserResponse();
    }

    /**
     * Create an instance of {@link RegistryUser }
     * 
     */
    public RegistryUser createRegistryUser() {
        return new RegistryUser();
    }

    /**
     * Create an instance of {@link RegistryUserResponse }
     * 
     */
    public RegistryUserResponse createRegistryUserResponse() {
        return new RegistryUserResponse();
    }

    /**
     * Create an instance of {@link UpdateUserById }
     * 
     */
    public UpdateUserById createUpdateUserById() {
        return new UpdateUserById();
    }

    /**
     * Create an instance of {@link UpdateUserByIdResponse }
     * 
     */
    public UpdateUserByIdResponse createUpdateUserByIdResponse() {
        return new UpdateUserByIdResponse();
    }

    /**
     * Create an instance of {@link UpdateUserByLogin }
     * 
     */
    public UpdateUserByLogin createUpdateUserByLogin() {
        return new UpdateUserByLogin();
    }

    /**
     * Create an instance of {@link UpdateUserByLoginResponse }
     * 
     */
    public UpdateUserByLoginResponse createUpdateUserByLoginResponse() {
        return new UpdateUserByLoginResponse();
    }

    /**
     * Create an instance of {@link UpdateUserPassword }
     * 
     */
    public UpdateUserPassword createUpdateUserPassword() {
        return new UpdateUserPassword();
    }

    /**
     * Create an instance of {@link UpdateUserPasswordResponse }
     * 
     */
    public UpdateUserPasswordResponse createUpdateUserPasswordResponse() {
        return new UpdateUserPasswordResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "clearUser")
    public JAXBElement<ClearUser> createClearUser(ClearUser value) {
        return new JAXBElement<ClearUser>(_ClearUser_QNAME, ClearUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "clearUserResponse")
    public JAXBElement<ClearUserResponse> createClearUserResponse(ClearUserResponse value) {
        return new JAXBElement<ClearUserResponse>(_ClearUserResponse_QNAME, ClearUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "existsUserByEmail")
    public JAXBElement<ExistsUserByEmail> createExistsUserByEmail(ExistsUserByEmail value) {
        return new JAXBElement<ExistsUserByEmail>(_ExistsUserByEmail_QNAME, ExistsUserByEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "existsUserByEmailResponse")
    public JAXBElement<ExistsUserByEmailResponse> createExistsUserByEmailResponse(ExistsUserByEmailResponse value) {
        return new JAXBElement<ExistsUserByEmailResponse>(_ExistsUserByEmailResponse_QNAME, ExistsUserByEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "existsUserByLogin")
    public JAXBElement<ExistsUserByLogin> createExistsUserByLogin(ExistsUserByLogin value) {
        return new JAXBElement<ExistsUserByLogin>(_ExistsUserByLogin_QNAME, ExistsUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "existsUserByLoginResponse")
    public JAXBElement<ExistsUserByLoginResponse> createExistsUserByLoginResponse(ExistsUserByLoginResponse value) {
        return new JAXBElement<ExistsUserByLoginResponse>(_ExistsUserByLoginResponse_QNAME, ExistsUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "findAllUser")
    public JAXBElement<FindAllUser> createFindAllUser(FindAllUser value) {
        return new JAXBElement<FindAllUser>(_FindAllUser_QNAME, FindAllUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "findAllUserResponse")
    public JAXBElement<FindAllUserResponse> createFindAllUserResponse(FindAllUserResponse value) {
        return new JAXBElement<FindAllUserResponse>(_FindAllUserResponse_QNAME, FindAllUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "findUserById")
    public JAXBElement<FindUserById> createFindUserById(FindUserById value) {
        return new JAXBElement<FindUserById>(_FindUserById_QNAME, FindUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "findUserByIdResponse")
    public JAXBElement<FindUserByIdResponse> createFindUserByIdResponse(FindUserByIdResponse value) {
        return new JAXBElement<FindUserByIdResponse>(_FindUserByIdResponse_QNAME, FindUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "findUserByLogin")
    public JAXBElement<FindUserByLogin> createFindUserByLogin(FindUserByLogin value) {
        return new JAXBElement<FindUserByLogin>(_FindUserByLogin_QNAME, FindUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "findUserByLoginResponse")
    public JAXBElement<FindUserByLoginResponse> createFindUserByLoginResponse(FindUserByLoginResponse value) {
        return new JAXBElement<FindUserByLoginResponse>(_FindUserByLoginResponse_QNAME, FindUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "getCurrentUser")
    public JAXBElement<GetCurrentUser> createGetCurrentUser(GetCurrentUser value) {
        return new JAXBElement<GetCurrentUser>(_GetCurrentUser_QNAME, GetCurrentUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "getCurrentUserResponse")
    public JAXBElement<GetCurrentUserResponse> createGetCurrentUserResponse(GetCurrentUserResponse value) {
        return new JAXBElement<GetCurrentUserResponse>(_GetCurrentUserResponse_QNAME, GetCurrentUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "registryUser")
    public JAXBElement<RegistryUser> createRegistryUser(RegistryUser value) {
        return new JAXBElement<RegistryUser>(_RegistryUser_QNAME, RegistryUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "registryUserResponse")
    public JAXBElement<RegistryUserResponse> createRegistryUserResponse(RegistryUserResponse value) {
        return new JAXBElement<RegistryUserResponse>(_RegistryUserResponse_QNAME, RegistryUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "updateUserById")
    public JAXBElement<UpdateUserById> createUpdateUserById(UpdateUserById value) {
        return new JAXBElement<UpdateUserById>(_UpdateUserById_QNAME, UpdateUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "updateUserByIdResponse")
    public JAXBElement<UpdateUserByIdResponse> createUpdateUserByIdResponse(UpdateUserByIdResponse value) {
        return new JAXBElement<UpdateUserByIdResponse>(_UpdateUserByIdResponse_QNAME, UpdateUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "updateUserByLogin")
    public JAXBElement<UpdateUserByLogin> createUpdateUserByLogin(UpdateUserByLogin value) {
        return new JAXBElement<UpdateUserByLogin>(_UpdateUserByLogin_QNAME, UpdateUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "updateUserByLoginResponse")
    public JAXBElement<UpdateUserByLoginResponse> createUpdateUserByLoginResponse(UpdateUserByLoginResponse value) {
        return new JAXBElement<UpdateUserByLoginResponse>(_UpdateUserByLoginResponse_QNAME, UpdateUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "updateUserPassword")
    public JAXBElement<UpdateUserPassword> createUpdateUserPassword(UpdateUserPassword value) {
        return new JAXBElement<UpdateUserPassword>(_UpdateUserPassword_QNAME, UpdateUserPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.golovina.tsc.ru/", name = "updateUserPasswordResponse")
    public JAXBElement<UpdateUserPasswordResponse> createUpdateUserPasswordResponse(UpdateUserPasswordResponse value) {
        return new JAXBElement<UpdateUserPasswordResponse>(_UpdateUserPasswordResponse_QNAME, UpdateUserPasswordResponse.class, null, value);
    }

}
