package ru.tsc.golovina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Session;
import ru.tsc.golovina.tm.model.Task;

import javax.jws.WebParam;
import java.util.List;

public interface ITaskService extends IOwnerService<Task> {

}
