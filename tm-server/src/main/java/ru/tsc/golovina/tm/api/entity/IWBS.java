package ru.tsc.golovina.tm.api.entity;

public interface IWBS extends IHasCreated, IHasStartDate, IHasName, IHasStatus {
}
