package ru.tsc.golovina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.Session;
import ru.tsc.golovina.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @WebMethod
    void add(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") User entity
    );

    @WebMethod
    void addAll(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entities") List<User> entities
    );

    @WebMethod
    void remove(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") User entity
    );

    @WebMethod
    @NotNull List<User> findAll(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    boolean existsById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    boolean existsByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    );

    @WebMethod
    void clear(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull User findById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @Nullable User findByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    );

    @WebMethod
    @Nullable User removeById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @Nullable User removeByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    );

    @WebMethod
    public @NotNull User removeByLogin(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    );

    @WebMethod
    @NotNull Integer getSize(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull User create(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password
    );

    @WebMethod
    @NotNull User createWithEmail(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "email") String email
    );

    @WebMethod
    @NotNull User createWithRole(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "role") Role role
    );

    @WebMethod
    void setPassword(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "password") String password
    );

    @WebMethod
    void setRole(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "role") Role role
    );

    @WebMethod
    @NotNull User findUserByLogin(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String login
    );

    @WebMethod
    @NotNull User findUserByEmail(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "email") String email
    );

    @WebMethod
    boolean isLoginExists(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    );

    @WebMethod
    boolean isEmailExists(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "email") String email
    );

    @WebMethod
    void updateUserByLogin(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "lastName", partName = "lastName") final String lastName,
            @WebParam(name = "middleName", partName = "middleName") final String middleName,
            @WebParam(name = "email", partName = "email") final String email
    );

    @WebMethod
    void updateUserById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "lastName", partName = "lastName") final String lastName,
            @WebParam(name = "middleName", partName = "middleName") final String middleName,
            @WebParam(name = "email", partName = "email") final String email
    );

}
