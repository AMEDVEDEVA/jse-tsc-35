package ru.tsc.golovina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.Session;
import ru.tsc.golovina.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    void updateUserPassword(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "password", partName = "password") final String password
    );

}
