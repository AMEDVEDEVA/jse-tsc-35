package ru.tsc.golovina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.endpoint.IUserEndpoint;
import ru.tsc.golovina.tm.api.service.ISessionService;
import ru.tsc.golovina.tm.api.service.IUserService;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IUserService userService;

    public UserEndpoint() {
    }

    public UserEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IUserService userService
                             ) {
        this.sessionService = sessionService;
        this.userService = userService;
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "password") final String password
    ) {
        sessionService.validate(session);
        userService.setPassword(session.getUserId(), password);
    }

}
